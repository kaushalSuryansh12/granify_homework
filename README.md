# Assignment

## Pet Shop App

## Description
This is a pet shop app created entirely in javascript. There are several functionalities in the app. Two types of pets can be added to the pet shop. You can set name, age, favorite food to the your pet and change it later on according to your requirements. You can get access to all the names that a pet has, change its sound and insert the pet information into the database.

## Installation
Accessing the app is easy. Download the zip folder and unzip it. There are few requirements for the app to work on your computer. First you should have node js installed on your computer. Secondly, you can open the project via VS Code and start the terminal inside the project or use cmd, change your directory to being inside the project folder. Afterwards the enitre application can be run by one single command npm run dev which run several scripts one by one. Here is what I mean `D:\granify_assignment>npm run dev`. The first being installing node modules (jest) required for the application. Followed by the script from main.js which happens to check the functionality of cat and dog classes, then the third script being petshop.js which inserts multiple instantiated object into the database. The final script that will be run is unit tests for the getAge() and speak() method. You can see the executed results in the terminal.

## File Structure
The file has three folders controllers, components, model along main.js, petshop.js and package.json files. Functionality related to dogs and cats are inside the components folder, Database functions are in the controllers folder and the sample mySql statements are in the model folder. main.js and petshop.js which are the executable scripts are in the main folder. package.json file has information related to project including run scripts and dependencies.

## Functionality
Instantiated objects for cats and dogs are using methods and insert functionalities to change their properties as well as insert into the fake db which is just an array of two objects(cats and dogs). Dogs and Cats and inserted into the database in respect to the tables present. All the three scripts are run by a single command which displays all the functionality displayed to the terminal.

## Visuals
`-------------------
-----DOG DATA------
-------------------
1. Name is currently Bruno
2. Name has been changed to Sunny
3. All the names the dog has ever had Bruno,Sunny
4. Average name length of cat is 5
-------------------
-----CAT DATA------
-------------------
1. Name is currently Katrina
2. Name has been changed to Surii
3. All the names the cat has ever had Katrina,Surii
4. Average name length of cat is 6
-------------------
--INSERTING DATA---
-------------------
Connecting to animalsDB

Inserting "Sunny" into table Dogs


Inserting "Surii" into table Cats

-------------------
Fetchin table Dogs
-------------------

DATABASE: animalsDB, TABLE : Dogs
{ Dogs: [ { Pet_name: 'Sunny', Age: 7, Favorite_food: 'milk' } ] }

-------------------
Fetchin table Cats
-------------------

DATABASE: animalsDB, TABLE : Cats
{ Cats: [ { Pet_name: 'Surii', Age: 5, Favorite_food: 'beef' } ] }

Connecting to animalsDB

Inserting "Arneet" into table Cats

Inserting "Bruno" into table Dogs

Inserting "" into table Cats

Inserting "" into table Cats

Inserting "" into table Cats

Inserting "" into table Dogs

Inserting "" into table Dogs

Inserting "" into table Dogs

-------------------
Fetchin table Dogs
-------------------

DATABASE: animalsDB, TABLE : Dogs
{
  Dogs: [
    { Pet_name: 'Bruno', Age: 5, Favorite_food: 'chicken' },
    { Pet_name: '', Age: 7, Favorite_food: 'chicken' },
    { Pet_name: '', Age: 7, Favorite_food: 'milk' },
    { Pet_name: '', Age: 7, Favorite_food: 'peanut butter' }
  ]
}

-------------------
Fetchin table Cats
-------------------

DATABASE: animalsDB, TABLE : Cats
{
  Cats: [
    { Pet_name: 'Arneet', Age: 8, Favorite_food: 'ham' },
    { Pet_name: '', Age: 7, Favorite_food: 'milk' },
    { Pet_name: '', Age: 7, Favorite_food: 'ham' },
    { Pet_name: '', Age: 7, Favorite_food: 'salmon' }
  ]
}

> granify_assignment@1.0.0 test
> jest

 PASS  unit_tests/speak.test.js
 PASS  unit_tests/cat.test.js

Test Suites: 2 passed, 2 total
Tests:       2 passed, 2 total
Snapshots:   0 total
Time:        1.685 s, estimated 2 s
Ran all test suites.`




